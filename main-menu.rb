{
  "menu"=> [
    {
      "id"=> "inicio",
      "href"=> "/",
      "label"=> "Início",
      "sub_items"=> [],
      "is_visible"=> true
    },
    {
      "id"=> "ppa",
      "href"=> "javascript:void(0)",
      "label"=> "PPA Participativo",
      "sub_items"=> [
        {
          "id"=> "ppa-inicio",
          "href"=> "/processes/programas/f/45/",
          "label"=> "Memória",
          "is_visible"=> true
        },
        {
          "id"=> "ppa-relatorios",
          "href"=> "/pages/relatoriodaplataforma",
          "label"=> "Relatórios",
          "is_visible"=> true
        },
        {
          "id"=> "ppa-programas",
          "href"=> "/processes/programas/f/1/",
          "label"=> "Programas priorizados",
          "is_visible"=> true
        },
        {
          "id"=> "ppa-propostas",
          "href"=> "/processes/programas/f/2/",
          "label"=> "Propostas mais votadas",
          "is_visible"=> true
        },
        {
          "id"=> "ppa-sobre",
          "href"=> "/pages",
          "label"=> "Entenda o processo",
          "is_visible"=> true
        }
      ],
      "is_visible"=> true
    },
    {
      "id"=> "conferencias",
      "href"=> "javascript:void(0)",
      "label"=> "Conferências",
      "sub_items"=> [
        {
          "id"=> "juventude",
          "href"=> "javascript:void(0)",
          "label"=> "Juventude",
          "sub_items"=> [
            {
              "id"=> "juventude-inicio",
              "href"=> "/assemblies/confjuv4/f/34/",
              "label"=> "Início",
              "is_visible"=> true
            },
            {
              "id"=> "juventude-etapas",
              "href"=> "/assemblies/confjuv4/f/14/",
              "label"=> "Etapas",
              "is_visible"=> true
            },
            {
              "id"=> "juventude-etapa-digital",
              "href"=> "/assemblies/confjuv4/f/10/",
              "label"=> "Etapa Digital",
              "is_visible"=> true
            },
            {
              "id"=> "juventude-noticias",
              "href"=> "/assemblies/confjuv4/f/9/",
              "label"=> "Notícias",
              "is_visible"=> true
            },
            {
              "id"=> "juventude-mobilizacao",
              "href"=> "/assemblies/confjuv4/f/42/posts/44",
              "label"=> "Mobilização",
              "is_visible"=> true
            },
            {
              "id"=> "juventude-perguntas-frequentes",
              "href"=> "/assemblies/confjuv4/f/13/",
              "label"=> "Sobre",
              "is_visible"=> true
            }
          ],
          "is_visible"=> true
        },
        {
          "id"=> "seguranca-alimentar",
          "href"=> "javascript:void(0)",
          "label"=> "Segurança alimentar",
          "sub_items"=> [
            {
              "id"=> "seguranca-alimentar-inicio",
              "href"=> "/assemblies/cnsan6/f/41/",
              "label"=> "Início",
              "is_visible"=> true
            },
            {
              "id"=> "seguranca-alimentar-etapas",
              "href"=> "/assemblies/cnsan6/f/18",
              "label"=> "Etapas",
              "is_visible"=> true
            },
            {
              "id"=> "seguranca-alimentar-noticias",
              "href"=> "/assemblies/cnsan6/f/20",
              "label"=> "Notícias",
              "is_visible"=> true
            },
            {
              "id"=> "seguranca-alimentar-frequentes",
              "href"=> "/assemblies/cnsan6/f/36/",
              "label"=> "Perguntas Frequentes",
              "is_visible"=> true
            }
          ],
          "is_visible"=> true
        }
      ],
      "is_visible"=> true
    },
    {
      "id"=> "novidades",
      "href"=> "/processes/brasilparticipativo/f/26/posts",
      "label"=> "Novidades",
      "sub_items"=> [],
      "is_visible"=> true
    },
    {
      "id"=> "sobre",
      "href"=> "/processes/brasilparticipativo/f/33/",
      "label"=> "Sobre",
      "sub_items"=> [],
      "is_visible"=> true
    }
  ]
}
